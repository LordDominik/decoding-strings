import unittest
import lab


class TestInput(unittest.TestCase):

    def test_type_of_data(self):
        """
        Тестирование на типы данных.
        """

        self.assertIsInstance(lab.check_input('20'), str)
        self.assertEqual(lab.check_input('20'), '20')
        self.assertRaises(ValueError, lab.check_input, 'String')
        self.assertRaises(ValueError, lab.check_input, ' F ')
        self.assertRaises(ValueError, lab.check_input, '10.25')

    def test_unsuitable_data(self):
        """
        Тестирование на неподходящие значения.
        """

        self.assertRaises(ValueError, lab.check_input, '1')
        self.assertRaises(ValueError, lab.check_input, '123')
        self.assertRaises(ValueError, lab.check_input, '-12')
        self.assertRaises(ValueError, lab.check_input, '')

    def test_range(self):
        """
        Тестирование на допустимые диапазоны.
        """

        self.assertEqual(lab.check_input('00'), '00')
        self.assertEqual(lab.check_input('42'), '42')
        self.assertRaises(ValueError, lab.check_input, '0909')
        self.assertRaises(ValueError, lab.check_input, '43')
        self.assertRaises(ValueError, lab.check_input, '51')


if __name__ == '__main__':
    unittest.main()
