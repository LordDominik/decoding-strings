import unittest
import lab


class TestDecode(unittest.TestCase):

    def test_structure(self):
        """
        Тестирование на тип и размер.
        """

        self.assertIsInstance(lab.str_decode('0000'), str)
        self.assertEqual(len(lab.str_decode('000000')), 3)

    def test_mean_words(self):
        """
        Тестирование на осмысленные слова.
        """

        self.assertEqual(lab.str_decode('2300330415'), 'PAVEL')
        self.assertEqual(lab.str_decode('20000321043030'), 'MADNESS')

    def test_em_mean_words(self):
        """
        Тестирование на буквы и пробелы.
        """

        self.assertEqual(lab.str_decode('004235'), 'A X')
        self.assertEqual(lab.str_decode('22250142'), 'ORB ')


if __name__ == '__main__':
    unittest.main()
