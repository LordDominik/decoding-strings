def check_input(value):
    """
    Проверяет входные данные на коректность значений.

    Для корректной работы программы, введенная строка должна:
    1) иметь четную длину (т.к. идет работа с парами)
    2) состоять из чисел
    3) иметь только положительные числа
    4) иметь пары, которые соответствуют диапазону значений согласно ТЗ.

    Args:
        value:  Зашифрованная строка, введенная пользователем.

    Returns:
        Введенная строка, успешно прошедшая проверку.

    Raises:
        ValueError.

    Examples:
        >>> check_input('2300330415')
        '2300330415'
        >>> check_input('String')
        Traceback (most recent call last):
            ...
        ValueError: invalid literal for int() with base 10: 'String'
        >>> check_input('55111111')
        Traceback (most recent call last):
            ...
        ValueError
    """

    if len(value) % 2 == 0:

        if int(value) >= 0:

            for i in range(0, len(value)):
                v = int(value[i])

                if i % 2 == 0:
                    if v <= 3:
                        continue
                    if v == 4:
                        if int(value[i + 1]) <= 2:
                            continue
                else:
                    if v <= 5:
                        continue
                raise ValueError

            return value

    raise ValueError


def str_decode(e_str):
    """
    Расшифровывает введенную строку согласно представленному алгоритму.

    Сначало создается эквивалент таблицы расшифровки - двумерный массив с английскими буквами и пробелом
    Затем функция сравнивает пары элементов введенной строки с парой индексов массива
    (i- строка массива, j - столбей массива) и по полученным символам строит новую строку.

    Args:
        e_str: Введенная пользователем строка, которая должна быть проверена на корректность функцией check_input().

    Returns:
        Расшифрованная строка.

    Raises:
        ValueError, IndexError.


    Examples:
        >>> str_decode('-1234')
        Traceback (most recent call last):
            ...
        ValueError: invalid literal for int() with base 10: '-'
        >>> str_decode('661111')
        Traceback (most recent call last):
            ...
        IndexError: list index out of range
        >>> str_decode('')
        ''
        >>> str_decode('2300330415')
        'PAVEL'
    """

    enc_matrix = [[chr(65 + j + 6 * i) for j in range(0, 6) if 65 + j + 6 * i < 91] for i in range(0, 5)]
    enc_matrix[-1].append(' ')

    d_str = ''
    for i in range(0, len(e_str) - 1, 2):
        d_str += enc_matrix[int(e_str[i])][int(e_str[i + 1])]

    return d_str


def main():
    try:
        encrypted_word = check_input(input('Введите зашифрованную строку: '))
    except ValueError:
        print('Введенная строка не поддается дешифровке!')
        return

    print('Расшифрованная строка: ' + str_decode(encrypted_word))


if __name__ == '__main__':
    main()
